import encodeQuery from "../../common/utils/encodeQuery";
import {makeRequest} from "../../common/utils/axios";

const initialState = {
    records: []
}

const testTakers = {
    state: initialState,
    reducers: {
        setTestersList(state, payload) {
            return {...state, records: payload}
        },
    },
    effects: dispatch => ({
        async getTestersList() {
            dispatch.common.toggleLoading(true);
            try {
                const queryParam = encodeQuery({url: '/users?', params: {limit: 20, offset: 0}})
                const res = await makeRequest({url: queryParam});
                dispatch.testTakers.setTestersList(res);
            } catch (e) {
                dispatch.common.showError(e.response.statusText)
            } finally {
                dispatch.common.toggleLoading(false);
            }
        },
    }),
}

export default testTakers;