const initialState = {
    error: false,
    errorText: '',
    loading: false
}

const common = {
    state: initialState,
    reducers: {
        showError(state, payload) {
            return {...state, error: true, errorText: payload}
        },
        hideError(state) {
            return {...state, error: false}
        },
        toggleLoading(state, payload) {
            return {...state, loading: payload}
        }
    }
}

export default common;