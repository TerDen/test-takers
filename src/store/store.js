import { init } from '@rematch/core'
import {testTakers, common} from "../core/models";

const store = init( {
    models: {
        common,
        testTakers,
    }
})

export default store