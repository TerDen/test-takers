import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import {List} from "../../components";
import styles from './TestersListContainer.module.scss'
import {v4 as uuidv4} from "uuid";

const TestersListContainer = props => {
    const {testTakers, getTestTakers} = props;
    useEffect(() => {
        (async () => {
            await getTestTakers()
        })()
    }, [getTestTakers])

    const renderRow = (item) => {
        let row = '';
        ['firstName', 'lastName'].forEach(key => {
            if (!item[key]) return '';
            row = `${row} ${item[key]}`
        })
        return (
            <li key={uuidv4()} className={styles.row}>
                {row}
            </li>
        )
    }

    return (
        testTakers.length ?
            <List data={testTakers}
                  customRenderRow={renderRow}
                  customStyles={styles.list}/> : null
    )
}

const mapState = (state) => ({
    testTakers: state.testTakers.records,
})

const mapDispatch = dispatch => ({
    getTestTakers: dispatch.testTakers.getTestersList,
})

export default connect(mapState, mapDispatch)(TestersListContainer)