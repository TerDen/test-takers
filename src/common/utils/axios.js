import config from "../../config.js";
import axios from "axios";
import csvToJson from 'convert-csv-to-json';

const axiosClient = axios.create({
    baseURL: config.baseURL
});

axiosClient.interceptors.request.use(function (config) {
    return config;
});

export function makeRequest({url, type = 'GET', properties = {}}) {
    return axiosClient.request({
        method: type,
        url,
        ...properties
    }).then(response => {
        const contentType = response.headers['content-type'];
        switch (contentType) {
            case 'application/json;charset=utf-8':
                return response.data;
            case 'text/csv':
                return csvToJson.getJsonFromCsv(response.data);
            default:
                return response.data
        }
    });
}
