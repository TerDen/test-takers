import React, {useEffect} from 'react';
import {TestersListContainer} from "./containers";
import {connect} from "react-redux";
import {Preloader, Header} from "./components";

const App = props => {
    const {loading, error, errorText, hideError} = props;
    useEffect(() => {
        if (!error) return;
        alert(errorText);
        hideError();
    }, [error, errorText, hideError])
    return (
        <div className="App">
            <Header/>
            <main>
                <TestersListContainer/>
            </main>
            <Preloader isShow={loading} fullscreen={false}/>
        </div>
    );
}

const mapState = (state) => ({
    loading: state.common.loading,
    error: state.common.error,
    errorText: state.common.errorText,
})

const mapDispatch = dispatch => ({
    hideError: dispatch.common.hideError,
})

export default connect(mapState, mapDispatch)(App)
