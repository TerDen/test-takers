import React from 'react';
import {v4 as uuidv4} from "uuid";
import styles from "../../containers/TestersListContainer/TestersListContainer.module.scss";

const List = props => {
    const {data = [], customRenderRow, customStyles} = props;

    let renderRow = (item) => {
        let row = ''
        Object.keys(item).forEach(key => {
            row = `${row} ${key}: ${item[key]},`
        });

        return (
            <li key={uuidv4()} className={styles.row}>
                {row}
            </li>
        )
    }

    if (customRenderRow) renderRow = customRenderRow;

    return (
        <ul className={customStyles}>
            {
                data.map(item => {
                    return renderRow(item)
                })
            }
        </ul>
    )
}

export default List