import React from 'react';
import PropTypes from 'prop-types';

import styles from './Preloader.module.scss';

const Preloader = ({isShow, fullscreen = true}) => {
  if (Array.isArray(isShow) && isShow.length === 0) return null;
  if (!isShow) return null;
  return (
    <div className={styles.preloaderComponent} style={!fullscreen ? {position: 'absolute'} : {}}>
      <div>&nbsp;</div>
      <div className={styles.wrapper} style={!fullscreen ? {position: 'absolute'} : {}}>
        <div className={styles.spinner}>
          <div className={styles.tail}/>
        </div>
      </div>
    </div>
  );
};

Preloader.propTypes = {
  isShow: PropTypes.bool,
  fullscreen: PropTypes.bool
};

export default Preloader;
